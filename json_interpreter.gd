extends Node

var data = {}
var h3 = {}
var year = {}
var code = {}
var h3_metadata = {}
var crime_maxima = {}
var num_items = 0
var sorted_years = []

#json digest structure:
#number of h3 items
#years, sorted for easy low-hi recognition
#h3 metadata (h3 -> [id, xy coordinate])
#maximum instance of crimes (within a given year)
#crimes, organized by h3
#crimes, organized by year
#crimes, organized by crime code

# {"digest_data":[{"quantity":quantity},{"h3_metadata":h3_metadata},{"crime_maxima":crime_maxima},{"h3_primary":h3},{"year_primary":year},{"code_primary":code}]}


func _ready():
	json_to_data("res://crimes_new.json")
	
	organize_by_h3()
	organize_by_year()
	organize_by_code()
	
	var ind = 0
	
	for entry in data:
		if (!h3_metadata.keys().has(entry["h3"])):
			h3_metadata[entry["h3"]] = [ind, Vector2(entry["x"], entry["y"])]
			ind += 1
	
	for entry in code.keys():
		crime_maxima[entry] = {}
		for h3_ind in code[entry].keys():
			for year_ind in code[entry][h3_ind].keys():
				if(!crime_maxima[entry].keys().has(year_ind)):
					crime_maxima[entry][year_ind] = 0
				for hour in code[entry][h3_ind][year_ind].keys():
					crime_maxima[entry][year_ind] += code[entry][h3_ind][year_ind][hour]
	
	sorted_years = year.keys()
	sorted_years.sort()
	
	create_samples()
	
	var json_file = File.new()
	json_file.open("user://json/test.json", File.WRITE)
	
	var digest_data = {"digest_data":[{"quantity" : h3_metadata.keys().size()},
										{"h3_metadata" : h3_metadata},
										{"crime_maxima" : crime_maxima},
										{"h3_primary" : h3},
										{"year_primary" : year},
										{"code_primary" : code}]}
	json_file.store_string(JSON.print(digest_data))

#given a filepath, refresh the data container in this resource
func json_to_data(path):
	var file = File.new()
	file.open(path, File.READ)
	var file_contents = parse_json(file.get_as_text())
	data = file_contents["data"]

#the next three methods lag a little bit the first time you run them each time
#you refresh the data stored with a new json file
#as the dictionary must be created to be cached
#subsequent runs will provide the cached data though

func organize_by_h3():
	if (h3.size() > 0):
		return h3
	for item in data:
		if (!h3.has(item["h3"])):
			h3[item["h3"]] = {}
		if (!h3[item["h3"]].has(item["year"])):
			h3[item["h3"]][item["year"]] = {}
		if (!h3[item["h3"]][item["year"]].has(item["hour"])):
			h3[item["h3"]][item["year"]][item["hour"]] = {}
		if (!h3[item["h3"]][item["year"]][item["hour"]].has(item["crime_code"])):
			h3[item["h3"]][item["year"]][item["hour"]][item["crime_code"]] = 1
		else:
			h3[item["h3"]][item["year"]][item["hour"]][item["crime_code"]] += 1
	return h3

func organize_by_year():
	if (year.size() > 0):
		return year
	for item in data:
		if (!year.has(item["year"])):
			year[item["year"]] = {}
		if (!year[item["year"]].has(item["h3"])):
			year[item["year"]][item["h3"]] = {}
		if (!year[item["year"]][item["h3"]].has(item["hour"])):
			year[item["year"]][item["h3"]][item["hour"]] = {}
		if (!year[item["year"]][item["h3"]][item["hour"]].has(item["crime_code"])):
			year[item["year"]][item["h3"]][item["hour"]][item["crime_code"]] = 1
		else:
			year[item["year"]][item["h3"]][item["hour"]][item["crime_code"]] += 1
	return year

func organize_by_code():
	if (code.size() > 0):
		return code
	for item in data:
		if (!code.has(item["crime_code"])):
			code[item["crime_code"]] = {}
		if (!code[item["crime_code"]].has(item["h3"])):
			code[item["crime_code"]][item["h3"]] = {}
		if (!code[item["crime_code"]][item["h3"]].has(item["year"])):
			code[item["crime_code"]][item["h3"]][item["year"]] = {}
		if (!code[item["crime_code"]][item["h3"]][item["year"]].has(item["hour"])):
			code[item["crime_code"]][item["h3"]][item["year"]][item["hour"]] = 1
		else:
			code[item["crime_code"]][item["h3"]][item["year"]][item["hour"]] += 1
	return code

func create_samples():
	for entry in code:
		for ind in range(0, sorted_years.size(), 4):
			var ind1 = (ind + 1) if (ind + 1 < sorted_years.size()) else -1
			var ind2 = (ind + 2) if (ind + 2 < sorted_years.size()) else -1
			var ind3 = (ind + 3) if (ind + 3 < sorted_years.size()) else -1
			var image = Image.new()
			
			var pixels = []
			for _i in h3_metadata.keys().size():
				pixels.append(Color(0.0, 0.0, 0.0, 0.0))
			
			for h3_ind in code[entry]:
				var color = Color(0.0, 0.0, 0.0, 0.0)
				if (code[entry][h3_ind].keys().has(sorted_years[ind])):
					prints("r:", float(get_aggregate(code[entry][h3_ind][sorted_years[ind]])) / float(crime_maxima[entry][sorted_years[ind]]))
					color.r = float(get_aggregate(code[entry][h3_ind][sorted_years[ind]])) / float(crime_maxima[entry][sorted_years[ind]])
				if (code[entry][h3_ind].keys().has(sorted_years[ind1])):
					prints("g:", float(get_aggregate(code[entry][h3_ind][sorted_years[ind1]])) / float(crime_maxima[entry][sorted_years[ind1]]))
					color.g = float(get_aggregate(code[entry][h3_ind][sorted_years[ind1]])) / float(crime_maxima[entry][sorted_years[ind1]])
				if (code[entry][h3_ind].keys().has(sorted_years[ind2])):
					prints("b:", float(get_aggregate(code[entry][h3_ind][sorted_years[ind2]])) / float(crime_maxima[entry][sorted_years[ind2]]))
					color.b = float(get_aggregate(code[entry][h3_ind][sorted_years[ind2]])) / float(crime_maxima[entry][sorted_years[ind2]])
				if (code[entry][h3_ind].keys().has(sorted_years[ind3])):
					prints("a:", float(get_aggregate(code[entry][h3_ind][sorted_years[ind3]])) / float(crime_maxima[entry][sorted_years[ind3]]))
					color.a = float(get_aggregate(code[entry][h3_ind][sorted_years[ind3]])) / float(crime_maxima[entry][sorted_years[ind3]])
				
				pixels[h3_metadata[h3_ind][0]] = color
			
			var compressed_array = []
			for pixel in pixels:
				compressed_array.append(pixel.r)
				compressed_array.append(pixel.g)
				compressed_array.append(pixel.b)
				compressed_array.append(pixel.a)
			
			var byte_arr = PoolByteArray(compressed_array)
			image.data = {"data" : byte_arr, "format" : "RGBA8", "height" : 1, "mipmaps" : false, "width" : h3_metadata.keys().size()}
			var first_h3
			var last_h3
			var final_year
			
			if (ind3 != -1):
				final_year = ind3
			elif (ind2 != -1):
				final_year = ind2
			elif (ind1 != -1):
				final_year = ind1
			else:
				final_year = ind
			
			for h3_ind in h3_metadata.keys():
				if (h3_metadata[h3_ind][0] == 0):
					first_h3 = h3_ind
				if (h3_metadata[h3_ind][0] == (h3_metadata.keys().size() - 1)):
					last_h3 = h3_ind
			
			image.save_png("user://pngs/" + entry + "-" + first_h3 + "-" + str(sorted_years[ind]) + "-" + str(sorted_years[final_year]) + ".png")

func get_aggregate(entry):
	var ret_val = 0
	for item in entry.values():
		ret_val += item
	
	return ret_val
