extends Node2D

var points = []

func begin_draw():
	update()

func _draw():
	for point in points:
		draw_circle(point, 10, Color.red)
	pass
