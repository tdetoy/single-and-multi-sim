tool
extends EditorScript

func _run():
	_random_three_channels()

func _export_to_image(data, _x, _y):
	
	#compress to 1D PBA
	var arr = Array()
	for x in range(_x):
		for y in range(_y):
			arr.append(data[x][y][0])
			arr.append(data[x][y][1])
			arr.append(data[x][y][2])
	print(arr)
	var byte_arr = PoolByteArray(arr)
	
	print(arr)
	print(byte_arr)
	
	var image = Image.new()
	image.data = {"data" : byte_arr, "format" : "RGB8", "height" : 30, "mipmaps" : false, "width" : 30}
	#argh i am VERY DUMB and OVERTHINK THINGS
	image.save_png("test_buffer.png")
	
	#verified that the raw values persist if not converted to linear
	image_to_poolbyte()
	#image_to_srgb()

func _random_three_channels():
	randomize()
	var noise1 = OpenSimplexNoise.new()
	var noise2 = OpenSimplexNoise.new()
	var noise3 = OpenSimplexNoise.new()
	
	noise1.seed = randi()
	noise2.seed = randi()
	noise3.seed = randi()
	
	var data = Array()
	
	var max1 = int(rand_range(10.0, 30.0))
	var max2 = int(rand_range(20.0, 150.0))
	var max3 = int(rand_range(15.0, 1250.0))
	
	for x in range(30):
		data.append(Array())
		for y in range(30):
			data[x].append(Array())
			#just a bunch of random numbers, only positive, only integers
			#no coorelation to real-life data, only for experimentation
			data[x][y].append(abs(noise1.get_noise_2d(x, y) * max1))
			data[x][y].append(abs(noise2.get_noise_2d(x, y) * max2))
			data[x][y].append(abs(noise3.get_noise_2d(x, y) * max3))
	
	_export_to_image(data, 30, 30)

#obsolete, but good for verifying that the raw data is not changed if the image
#is not converted (i.e. for imagetexture)
func image_to_poolbyte():
	var image = Image.new()
	image.load("test_buffer.png")
	var image_data = image.get_data()
	var hex_data = image_data.hex_encode()
	print(hex_data)
#	for entry in hex_data:
#		print(entry)

func image_to_srgb():
	var image = Image.new()
	image.load("test_buffer.png")
	var tex = ImageTexture.new()
	tex.create_from_image(image)
	
