extends PopupPanel

onready var multi_pass_panel = $"CenterContainer/VBoxContainer/TabContainer/Multi-Pass"
onready var single_pass_panel = $"CenterContainer/VBoxContainer/TabContainer/Single-Pass"

var single_pass_data
var multi_pass_data
var single_pass_rects
var multi_pass_rects

func _on_Button_pressed():
	queue_free()

func process_data(data, parameter_data):
	#process multi-pass first
	#get raw data for each cell
	multi_pass_data = Array()
	single_pass_data = Array()
	for x in data.size():
		multi_pass_data.append(Array())
		for y in data[x].size():
			var value = 0.0
			for p in data[x][y].size():
				value += data[x][y][p] * parameter_data[p].get_weight()
			multi_pass_data[x].append(value)
	
	#find minimum and maximum possible values adjusted by weight
	var _min = 0.0
	var _max = 0.0
	for p in parameter_data.size():
		_min += parameter_data[p].get_adj_min()
		_max += parameter_data[p].get_adj_max()
	
	#map values 0..1
	for x in multi_pass_data.size():
		for y in multi_pass_data.size():
			multi_pass_data[x][y] = map(multi_pass_data[x][y], _min, _max, 0.0, 1.0)
	
	#next, single-pass
	#map values 0..1 and add within loop, based on adj min/max values
	for x in data.size():
		single_pass_data.append(Array())
		for y in data[x].size():
			var weighted = 0.0
			_min = 0.0
			_max = 0.0
			for p in data[x][y].size():
				_min += parameter_data[p].get_adj_min()
				_max += parameter_data[p].get_adj_max()
				weighted += data[x][y][p] * parameter_data[p].get_weight()
			single_pass_data[x].append(map(weighted, _min, _max, 0.0, 1.0))
	
	draw_data()

func draw_data():
	single_pass_rects = Array()
	multi_pass_rects = Array()
	
	#single and multi pass should be same dimensions and square, so
	var iterations_side = single_pass_data.size()
	
	#same for panels
	var rect_dimension = single_pass_panel.rect_size.x / iterations_side
	
	for i in (iterations_side * iterations_side):
		single_pass_rects.append(Rect2(Vector2(int(i * rect_dimension) % int(single_pass_panel.rect_size.x), int((i * rect_dimension) / single_pass_panel.rect_size.x) * rect_dimension), Vector2(rect_dimension, rect_dimension)))
		multi_pass_rects.append(Rect2(Vector2(int(i * rect_dimension) % int(multi_pass_panel.rect_size.x), int((i * rect_dimension) / multi_pass_panel.rect_size.x) * rect_dimension), Vector2(rect_dimension, rect_dimension)))
		var single_label = Label.new()
		var multi_label = Label.new()
		single_pass_panel.add_child(single_label)
		multi_pass_panel.add_child(multi_label)
		single_label.rect_position = single_pass_rects[i].position
		multi_label.rect_position = multi_pass_rects[i].position
		single_label.text = "%1.1f" % single_pass_data[i % single_pass_data.size()][int(i / single_pass_data[0].size())]
		multi_label.text = "%1.1f" % multi_pass_data[i % multi_pass_data.size()][int(i / multi_pass_data[0].size())]
	
	update()
	

func _draw():
	if (single_pass_rects == null || multi_pass_rects == null):
		return 
	
	for i in single_pass_rects.size():
		single_pass_panel.get_rect_data(single_pass_rects[i], Color(single_pass_data[i % single_pass_data.size()][int(i / single_pass_data[0].size())], 0.0, 0.0))
	
	for k in multi_pass_rects.size():
		multi_pass_panel.get_rect_data(multi_pass_rects[k], Color(multi_pass_data[k % multi_pass_data.size()][int(k / multi_pass_data[0].size())], 0.0, 0.0))

func map(value, imin, imax, omin, omax):
	return (value - imin) * (omax - omin) / (imax - imin) + omin
