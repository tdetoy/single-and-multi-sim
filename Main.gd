extends Control

var results_panel = load("res://Results.tscn")

onready var size = $CenterContainer/VBoxContainer/HBoxContainer2/Size
onready var _min = $CenterContainer/VBoxContainer/HBoxContainer2/Min
onready var _max = $CenterContainer/VBoxContainer/HBoxContainer2/Max

onready var parameters = [
	$CenterContainer/VBoxContainer/Parameter,
	$CenterContainer/VBoxContainer/Parameter2,
	$CenterContainer/VBoxContainer/Parameter3,
	$CenterContainer/VBoxContainer/Parameter4,
	$CenterContainer/VBoxContainer/Parameter5
]

var simulated_area

func _ready():
	_build_data()

func _build_data():
	randomize()
	simulated_area = Array()
	
	#populate array
	for x in size.value:
		simulated_area.append(Array())
		for y in size.value:
			simulated_area[x].append(Array())
			for parameter in parameters:
				simulated_area[x][y].append(get_int_range(int(_min.value), int(_max.value)))
	
	#get minima and maxima
	for p in parameters.size():
		var par_min = 100000
		var par_max = 0
		for x in simulated_area.size():
			for y in simulated_area.size():
				par_min = min(par_min, simulated_area[x][y][p])
				par_max = max(par_max, simulated_area[x][y][p])
		parameters[p]._max = par_max
		parameters[p]._min = par_min
	
	
	print("done")

func _on_Randomize_pressed():
	_build_data()


func _on_Process_pressed():
	var new_results_panel = results_panel.instance()
	add_child(new_results_panel)
	new_results_panel.process_data(simulated_area, parameters)
	new_results_panel.popup_centered()

func get_int_range(_min, _max):
	return _min + (randi() % (_max - _min))
