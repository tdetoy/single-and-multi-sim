extends HBoxContainer

var _max setget set_max, get_max
var _min setget set_min, get_min
var _adj_min setget , get_adj_min
var _adj_max setget , get_adj_max

onready var _min_text = $Min
onready var _max_text = $Max
onready var _adj_min_text = $AdjMin
onready var _adj_max_text = $AdjMax
onready var _name = $Name
onready var _weight = $SpinBox

func _ready():
	pass

func _on_SpinBox_value_changed(value):
	_adj_max = _max * value
	_adj_min = _min * value
	_adj_min_text.text = "%6.1f" % _adj_min
	_adj_max_text.text = "%6.1f" % _adj_max

func get_min():
	return _min

func get_max():
	return _max

func get_adj_min():
	return _adj_min

func get_adj_max():
	return _adj_max

func get_weight():
	return _weight.value

func set_max(value):
	_max = value
	update_values()

func set_min(value):
	_min = value
	update_values()

func update_values():
	if (_min == null || _max == null):
		return
	_adj_min = _min * _weight.value
	_adj_max = _max * _weight.value
	
	_min_text.text = "%4d" % _min
	_max_text.text = "%4d" % _max
	_adj_min_text.text = "%6.1f" % _adj_min
	_adj_max_text.text = "%6.1f" % _adj_max
