extends Panel

var _rect
var _color

func get_rect_data(rect, color):
	_rect = rect
	_color = color
	update()

func _draw():
	draw_rect(_rect, _color)
