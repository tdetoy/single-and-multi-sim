tool
extends EditorScript

func _run():
	randomize()
	var number_runs = 1000000
	
	var original_array = []
	var translated_array = []
	
	var largest_value = 0
	var bonked = 0
	
	for i in range(number_runs):
		original_array.append(randi() % 4000)
		if (original_array[i] > largest_value):
			largest_value = original_array[i]
	
	for j in original_array.size():
		var inversed = inverse_lerp(0, largest_value, original_array[j])
		var lerped = int(lerp(0, largest_value, inversed))
		translated_array.append(lerped)
	
	for k in original_array.size():
		if (original_array[k] != translated_array[k]):
			prints("oops. original:",  original_array[k], "vs translated:", translated_array[k])
			bonked += 1
	
	prints("done.", bonked, "bonked translations, for a", (float(bonked) / float(original_array.size())) * 100, "bonkiness ratio")
